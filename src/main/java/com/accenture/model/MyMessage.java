package com.accenture.model;

import lombok.Data;

@Data
public class MyMessage {
    public String id;
    public String name;
    public String message;
}
