package com.accenture.camel;

import com.accenture.model.MyMessage;
import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.component.kafka.KafkaComponent;
import org.apache.camel.component.kafka.KafkaConfiguration;
import org.apache.camel.component.kafka.KafkaConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class KafkaRouteBuilder extends org.apache.camel.builder.RouteBuilder {

    private final Logger logger = LoggerFactory.getLogger(KafkaRouteBuilder.class);

    @Override
    public void configure() throws Exception {
        KafkaComponent component = new KafkaComponent();
        KafkaConfiguration config = new KafkaConfiguration();
        config.setBrokers("192.168.178.50:9092");
        config.setClientId("ACN-CLIENT-ID");
        config.setGroupId("ACN");
        config.setGroupInstanceId("Instance-Id");
        component.setConfiguration(config);
        CamelContext context = getContext();
        context.addComponent("kafka", component);
        Processor messageProcessor = new Processor() {
            @Override
            public void process(Exchange exchange) throws Exception {
                MyMessage body = exchange.getMessage().getBody(MyMessage.class);
                body.setMessage("Hello " + body.getName());

                exchange.getMessage().setBody(body);
            }
        };

        from("direct:myKafkaRoute")
                .id("myfirstCamelRoute")
                .setHeader("MyHeader", constant("something"))
                .setHeader(KafkaConstants.KEY, simple("${body.id}"))
                .log("Before: ${body}")
                .unmarshal().csv()
                .process(messageProcessor)
                .log("After: ${body}")
                .to("kafka:acn-test?brokers=192.168.178.50:9092");

        from("kafka:acn-test").id("Kafka Consumer Route")
                .log("Message received from Kafka : ${body}")
                .log("    on the topic ${headers[kafka.TOPIC]}")
                .log("    on the partition ${headers[kafka.PARTITION]}")
                .log("    with the offset ${headers[kafka.OFFSET]}")
                .log("    with the key ${headers[kafka.KEY]}");
        from("file:src/main/resources/toimport")
                .log("${body}")
                .to("file:src/main/resources/imported");
    }
}
