package com.accenture.camel;

import com.accenture.model.MyMessage;
import org.apache.camel.CamelContext;
import org.apache.camel.ProducerTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MyRestAPI {
    @Autowired
    CamelContext context;

    @PostMapping
    public @ResponseBody String postMessage(@RequestBody MyMessage message) {
        ProducerTemplate producerTemplate = context.createProducerTemplate();
        producerTemplate.sendBody("direct:myKafkaRoute", message);
        return null;
    }


}
